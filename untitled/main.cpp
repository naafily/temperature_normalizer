#include <fstream>
#include <iostream>
#include <semaphore.h>
#include <sstream>
#include <thread>

using namespace std;


#define FILE_PATH "../files/input.txt"
#define SENSORS_COUNT 10
#define STAGES_COUNT 20
#define INTERVAL 1


int sensor_values[SENSORS_COUNT];
sem_t mutex;
sem_t barrier;
sem_t barrier_mutex;
int counter = 0;
int barrier_counter = 0;

void empttize_or_create(const string &filepath) {
    ofstream outfile;
    outfile.open(filepath);
    outfile.close();

}

void append_to_file(const string &filepath, float value) {
    ofstream outfile;
    outfile.open(filepath, ios_base::app);
    outfile << value << ' ';
    outfile.close();
}

int get_max() {
    int return_value = sensor_values[0];
    for (int sensor_value : sensor_values)
        if (sensor_value > return_value)
            return_value = sensor_value;
    return return_value;
}


void read_sensor_value(int sensor_number) {
    ifstream data_file(FILE_PATH);
    int value = 0;
    string _;
    for (int i = 0; i < sensor_number; ++i)
        getline(data_file, _);
    empttize_or_create("../files/output/" + to_string(sensor_number));
    for (int i = 0; i < STAGES_COUNT; ++i) {
        cout << "Reading: " << sensor_number << endl;
        data_file >> value;
        sensor_values[sensor_number] = value;

        sem_wait(&mutex);
        counter++;
        sem_post(&mutex);

        if (counter == SENSORS_COUNT)
            sem_post(&barrier);
        sem_wait(&barrier);
        sem_wait(&barrier_mutex);
        barrier_counter++;
        sem_post(&barrier_mutex);
        if (barrier_counter != SENSORS_COUNT)
            sem_post(&barrier);
        cout << "getting max: " << sensor_number << endl;
        int max = get_max();
        append_to_file("../files/output/" + to_string(sensor_number), float(value) / max);
        counter = 0;
        this_thread::sleep_for(chrono::seconds(INTERVAL));
        barrier_counter = 0;
    }
}

int main() {
    thread threads[SENSORS_COUNT];
    sem_init(&mutex, 0, 1);
    sem_init(&barrier, 0, 0);
    sem_init(&barrier_mutex, 0, 1);
    for (int i = 0; i < SENSORS_COUNT; ++i)
        threads[i] = thread(read_sensor_value, i);
    for (auto &thread : threads)
        thread.join();
    return 0;
}
